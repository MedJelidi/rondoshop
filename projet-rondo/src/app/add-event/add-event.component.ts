import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {EventsService} from "../services/events.service";

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {

  eventForm: FormGroup;
  fileToUpload: File;
  loading: boolean;
  errorAdding: boolean = false;
  submitted: boolean;
  noPicture: boolean;
  debFin: boolean;
  exceedDate: boolean;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private eventsService: EventsService,
              private httpClient: HttpClient) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.eventForm = this.formBuilder.group(
      {
        name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
        location: ['', [Validators.required, Validators.maxLength(30)]],
        category: ['', Validators.required],
        description: ['', Validators.required],
        datedeb: ['', Validators.required],
        datefin: ['', Validators.required],
        image: ['', Validators.required]
      }
    );
  }

  handleFileInput(files: any) {
    this.fileToUpload = files.target.files[0];
    this.noPicture = false;
  }

  get f() { return this.eventForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.noPicture = typeof this.fileToUpload === 'undefined';
    if (this.eventForm.invalid) {
      return;
    }
    const formAddEventValue = this.eventForm.value;
    const name = formAddEventValue['name'];
    const location = formAddEventValue['location'];
    const category = formAddEventValue['category'];
    const datedeb = formAddEventValue['datedeb'];
    const datefin = formAddEventValue['datefin'];

    // <Verify dates> //

    const db_s = datedeb.split(/\D/);
    const df_s = datefin.split(/\D/);
    const db = new Date(db_s[0], --db_s[1], db_s[2]);
    const df = new Date(df_s[0], --df_s[1], df_s[2]);
    const dt = new Date();
    const daysDiff = (df.getTime() - db.getTime()) / (1000 * 3600 * 24);

    if ( (dt >= db) || (db >= df) ) {
      this.debFin = true;
      return;
    } else if (daysDiff > 14) {
      this.exceedDate = true;
      return;
    } else {
      this.debFin = false;
      this.exceedDate = false;
    }

    // </verify dates> //

    const description = formAddEventValue['description'];
    this.loading = true;
    const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
    const formData = new FormData();
    formData.append('nom', name);
    formData.append('lieu', location);
    formData.append('categorie', category);
    formData.append('dateheure_deb', datedeb);
    formData.append('dateheure_fin', datefin);
    formData.append('description', description);
    formData.append('image', this.fileToUpload);
    formData.append('idProp', idProp);
    this.httpClient.post<Event>('http://25.85.144.149/RESTapi/web/app_dev.php/events/new', formData)
      .subscribe(
        (response) => {
          console.log(response);
          this.router.navigate(['events']);
        },
        (error) => {
          console.log('Error while adding product to server: ' + error);
          this.errorAdding = true;
          this.loading = false;
        }
      );
  }

}
