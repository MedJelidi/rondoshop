export class Product {
  constructor(public id_product,
              public title,
              public price,
              public id_categorie,
              public color,
              public date_pub,
              public images,
              public rating,
              public id_prop,
              public description,
              public size,
              public valid,
              public city) {
  }
}
