export class Post {
  constructor(public id,
              public id_user,
              public shared_post,
              public content,
              public attached_file,
              public date_pub,
              public id_event,
              public share_count,
              public like_count,
              public valid) {
  }
}
