export class User {
  constructor(public id,
              public username,
              public username_canonical,
              public email,
              public email_canonical,
              public enabled,
              public password,
              public last_login,
              public roles,
              public gender,
              public phone,
              public block_exp,
              public block_r,
              public image,
              public city,
              public blocked
              ) {
  }
}
