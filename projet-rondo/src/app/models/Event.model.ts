export class Event {
  constructor(public id_event,
              public id_prop,
              public nom,
              public lieu,
              public image,
              public description,
              public dateheure_deb,
              public dateheure_fin,
              public dateheure_pub,
              public valid,
              public categorie) {
  }
}
