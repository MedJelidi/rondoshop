import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductsService} from "../services/products.service";
import { Product } from '../models/Product.model';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  productForm: FormGroup;
  fileToUpload: File[];
  isAdmin: boolean;
  loading: boolean;
  errorAdding: boolean = false;
  submitted: boolean;
  noPicture: boolean;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private productsService: ProductsService,
    private httpClient: HttpClient) { }

    ngOnInit() {
     this.initForm();
      if (localStorage.getItem('currentUserInfo') != null) {
        this.isAdmin = JSON.parse(localStorage.getItem('currentUserInfo'))[0].roles.includes("ROLE_ADMIN");
      } else {
        this.isAdmin = false;
      }
    }

    initForm() {
      this.productForm = this.formBuilder.group(
        {
          title: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
          price: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.maxLength(10)]],
          category: ['', Validators.required],
          size: ['', Validators.required],
          color: ['', Validators.required],
          description: ['', Validators.required],
          city: ['', [Validators.required, Validators.maxLength(30)]],
          'product[images][]': ['', Validators.required]
        }
      );
    }

  handleFileInput(files: any) {
      this.fileToUpload = files.target.files;
      this.noPicture = false;
  }

  get f() { return this.productForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.noPicture = typeof this.fileToUpload === 'undefined';
    if (this.productForm.invalid) {
      return;
    }
    this.loading = true;
    const formAddProductValue = this.productForm.value;
    const title = formAddProductValue['title'];
    const price = formAddProductValue['price'];
    const category = formAddProductValue['category'];
    const size = formAddProductValue['size'];
    const color = formAddProductValue['color'];
    const description = formAddProductValue['description'];
    const city = formAddProductValue['city'];
    const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;

    const formData = new FormData();
    formData.append('title', title);
    formData.append('price', price);
    formData.append('category', category);
    formData.append('size', size);
    formData.append('color', color);
    formData.append('description', description);
    formData.append('city', city);
    formData.append('idProp', idProp);
    for(let i = 0 ; i < this.fileToUpload.length ; i++) {
      formData.append('product[images][]', this.fileToUpload[i]);
     }

    this.httpClient.post<Product>('http://25.85.144.149/RESTapi/web/app_dev.php/products/new', formData)
      .subscribe(
        (response) => {
          console.log(response);
          if (this.isAdmin) {
            this.router.navigate(['products']);
          }
          this.router.navigate(['marketplace']);
        },
        (error) => {
          console.log('Error while adding product to server: ' + error);
          this.errorAdding = true;
          this.loading = false;
        }
      );
  }

}
