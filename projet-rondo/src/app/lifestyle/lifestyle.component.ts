import {Post} from "../models/Post.model";
import { Component, OnInit } from '@angular/core';
import {PostsService} from "../services/posts.service";

@Component({
  selector: 'app-lifestyle',
  templateUrl: './lifestyle.component.html',
  styleUrls: ['./lifestyle.component.css']
})
export class LifestyleComponent implements OnInit {

  postsForAdmin: Post[];
  postsForUser: Post[];
  stillLoading: boolean = true;
  isBlocked: boolean;
  isAdmin: boolean;
  i: number = 8;
  notscrolly: boolean = true;
  notEmptyPost: boolean = true;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsService.getPosts(0).subscribe(posts => {
      this.postsForUser = posts.filter(x => x.valid === 1);
      this.postsForAdmin = posts;
      this.stillLoading = false;
      if (localStorage.getItem('currentUserInfo') != null) {
        this.isBlocked = JSON.parse(localStorage.getItem('currentUserInfo'))[0].blocked === 1;
        this.isAdmin = JSON.parse(localStorage.getItem('currentUserInfo'))[0].roles.includes("ROLE_ADMIN");
      } else {
        this.isBlocked = false;
        this.isAdmin = false;
      }
    });
  }

  loadNextPosts() {
    this.postsService.getPosts(this.i)
      .subscribe( (posts) => {
        if (posts.length < 8) {
          this.notEmptyPost = false;
          this.stillLoading = false;
        }
        this.postsForAdmin = this.postsForAdmin.concat(posts.filter(x => x.valid === 1));
        this.postsForUser = this.postsForUser.concat(posts);
        this.i = this.i + 8;
        this.notscrolly = true;
      });
  }

  onScroll() {
    if (this.notscrolly && this.notEmptyPost) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextPosts();
    }
  }

}
