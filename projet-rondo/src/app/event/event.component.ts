import {Component, Input, OnInit} from '@angular/core';
import {EventsService} from "../services/events.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  @Input() title: string;
  @Input() loc: string;
  @Input() dat: string;
  @Input() id: number;
  @Input() img: string;
  @Input() desc: string;
  @Input() creator: string;
  @Input() valid: boolean;
  @Input() adminEvent: boolean;

  eventProfileStyle: boolean;
  isAdmin: boolean;

  constructor(private eventsService: EventsService,
              private httpClient: HttpClient,
              private router: Router) {
    this.eventProfileStyle = this.eventsService.eventProfileStyle;
  }

  ngOnInit() {
    if (localStorage.getItem('currentUserInfo') != null) {
      this.isAdmin = JSON.parse(localStorage.getItem('currentUserInfo'))[0].roles.includes("ROLE_ADMIN");
    } else {
      this.isAdmin = false;
    }
  }

  onDelete() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/events/' + this.id + '/delete')
      .subscribe(
        (response) => {
          console.log(response);
          this.router.navigate(['/events']);
        },
        (error) => {
          console.log('Error while deleting an event: ' + error.toString());
        }
      );
  }

  onVerify() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/events/' + this.id + '/accept')
      .subscribe(
        (response) => {
          this.router.navigate(['/events']);
        },
        (error) => {
          console.log('Error while verifying an event: ' + error.toString());
        }
      );
  }

  onBlock() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/events/' + this.id + '/block')
      .subscribe(
        (response) => {
          this.router.navigate(['/events']);
        },
        (error) => {
          console.log('Error while blocking an event: ' + error.toString());
        }
      );
  }

}
