import { Component, OnInit } from '@angular/core';
import {Product} from "../models/Product.model";
import {Event} from "../models/Event.model";
import {Post} from "../models/Post.model";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductsService} from "../services/products.service";
import {EventsService} from "../services/events.service";
import {PostsService} from "../services/posts.service";
import {UserService} from "../services/user.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userName: string = 'UserName';
  phoneNumber: string = '28249424';
  gender: string = 'Male';
  date: string = '2018-04-25';
  onEditClick: boolean = false;
  showEdit: boolean;
  onViewProductsClick: boolean = false;
  onViewEventsClick: boolean = false;
  onViewPostsClick: boolean = false;
  stillLoading: boolean = true;
  stillFullLoading: boolean = true;
  lastLogin: string;
  imgUrl: string = '/assets/img/icons/profile-picture.png';
  fileToUpload: File;

  verifiedProducts: Product[];
  unverifiedProducts: Product[];
  verifiedEvents: Event[];
  unverifiedEvents: Event[];
  verifiedPosts: Post[];
  unverifiedPosts: Post[];
  incorrectData: boolean;
  correctData: boolean;
  i_products: number = 8;
  i_events: number = 8;
  i_posts: number = 8;
  idUser: number;

  notscrolly: boolean = true;
  notEmptyProduct: boolean = true;
  notEmptyEvent: boolean = true;
  notEmptyPost: boolean = true;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private productsService: ProductsService,
              private eventsService: EventsService,
              private postsService: PostsService,
              private userService: UserService) {

    const id = this.route.snapshot.params['id'];
    this.userService.getUser(id).subscribe((response) => {
        if (JSON.parse(localStorage.getItem('currentUserInfo')) != null) {
          if (JSON.parse(localStorage.getItem('currentUserInfo'))[0].id == id) {
            this.showEdit = true;
          }
        }

        this.stillFullLoading = false;
        this.idUser = response.id;
        this.userName = response.username;
        this.phoneNumber = response.phone;
        this.gender = response.gender;
        this.lastLogin = response.last_login;
        this.imgUrl = response.image;

        this.productsService.getProducts(0).subscribe(
          (data) => {
            this.verifiedProducts = data.filter(x => x.id_prop.id === this.idUser && x.valid === 1);
            this.unverifiedProducts = data.filter(x => x.id_prop.id === this.idUser && x.valid === 0);
            this.stillLoading = false;
          }
        );

        this.eventsService.getEvents(0).subscribe(
          (data) => {
            this.verifiedEvents = data.filter(x => x.id_prop.id === this.idUser && x.valid === 1);
            this.unverifiedEvents = data.filter(x => x.id_prop.id === this.idUser && x.valid === 0);
            this.stillLoading = false;
          }
        );

        this.postsService.getPosts(0).subscribe(
          (data) => {
            this.verifiedPosts = data.filter(x => x.id_prop.id === this.idUser && x.valid === 1);
            this.unverifiedPosts = data.filter(x => x.id_prop.id === this.idUser && x.valid === 0);
            this.stillLoading = false;
          }
        );
      },
      (error) => {
        //this.router.navigate(['/not-found']);
      });

  }

  ngOnInit() { }

  handleFileInput(files: any) {
    this.fileToUpload = files.target.files[0];
    if (files.target.files && files.target.files[0]) {
      let reader = new FileReader();

      reader.readAsDataURL(files.target.files[0]); // read file as data url

      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.imgUrl = event.target.result;
        console.log("changed!");
      }
    }
  }

  onViewProducts() {
    this.onViewProductsClick = !this.onViewProductsClick;
    this.onViewEventsClick = false;
    this.onViewPostsClick = false;
    this.onEditClick = false;
    this.productsService.Edit = true;
  }

  onViewEvents() {
    this.onViewEventsClick = !this.onViewEventsClick;
    this.onViewProductsClick = false;
    this.onViewPostsClick = false;
    this.onEditClick = false;
    this.eventsService.eventProfileStyle = true;
  }

  onViewPosts() {
    this.onViewPostsClick = !this.onViewPostsClick;
    this.onViewProductsClick = false;
    this.onViewEventsClick = false;
    this.onEditClick = false;
    this.postsService.Edit = true;
  }

  onEdit() {
    this.onEditClick = true;
    this.onViewProductsClick = false;
    this.onViewEventsClick = false;
    this.onViewPostsClick = false;
  }

  onCancel() {
    this.onEditClick = false;
  }

  onSubmit(form: NgForm) {
    const username = form.value['username'];
    const phoneNumber = form.value['phonenumber'];
    const gender = form.value['gender'];
    const date = form.value['date'];
    const password = form.value['password'];
    if (password == this.userService.users[this.userService.loggedInUser].password) {
      this.correctData = true;
      this.incorrectData = false;
    } else {
      this.incorrectData = true;
      this.correctData = false;
    }
  }

  loadNextProducts() {
    this.productsService.getProducts(this.i_products)
      .subscribe( (products) => {
        if (products.length < 8) {
          this.notEmptyProduct = false;
          this.stillLoading = false;
        }

        this.verifiedProducts = this.verifiedProducts.concat(products.filter(x => x.id_prop.id === this.idUser && x.valid === 1));
        this.unverifiedProducts = this.unverifiedProducts.concat(products.filter(x => x.id_prop.id === this.idUser && x.valid === 0));
        this.i_products = this.i_products + 8;
        this.notscrolly = true;
      });
  }

  onScrollProducts() {
    if (this.notscrolly && this.notEmptyProduct) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextProducts();
    }
  }

  loadNextEvents() {
    this.eventsService.getEvents(this.i_events)
      .subscribe( (events) => {
        if (events.length < 8) {
          this.notEmptyEvent = false;
          this.stillLoading = false;
        }

        this.verifiedEvents = this.verifiedEvents.concat(events.filter(x => x.id_prop.id === this.idUser && x.valid === 1));
        this.unverifiedEvents = this.unverifiedEvents.concat(events.filter(x => x.id_prop.id === this.idUser && x.valid === 0));
        this.i_events = this.i_events + 8;
        this.notscrolly = true;
      });
  }

  onScrollEvents() {
    if (this.notscrolly && this.notEmptyEvent) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextEvents();
    }
  }

  loadNextPosts() {
    this.postsService.getPosts(this.i_posts)
      .subscribe( (posts) => {
        if (posts.length < 8) {
          this.notEmptyPost = false;
          this.stillLoading = false;
        }

        this.verifiedPosts = this.verifiedPosts.concat(posts.filter(x => x.id_prop.id === this.idUser && x.valid === 1));
        this.unverifiedPosts = this.unverifiedPosts.concat(posts.filter(x => x.id_prop.id === this.idUser && x.valid === 0));
        this.i_posts = this.i_posts + 8;
        this.notscrolly = true;
      });
  }

  onScrollPosts() {
    if (this.notscrolly && this.notEmptyPost) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextPosts();
    }
  }

}
