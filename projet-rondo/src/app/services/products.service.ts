import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product} from "../models/Product.model";
import {NavigationStart, Router} from "@angular/router";

@Injectable()
export class ProductsService {

  Edit: boolean = false;
  inCatalog: boolean;
  inMarketplace: boolean;
  inProfile: boolean;
  products: Product[];
  isblocked: boolean;

  constructor(private httpClient: HttpClient, private router: Router) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'].includes('products')) {
          this.inCatalog = true;
          this.inMarketplace = false;
          this.inProfile = false;
        } else if (event['url'].includes('marketplace')) {
          this.inMarketplace = true;
          this.inCatalog = false;
          this.inProfile = false;
        } else if (event['url'].includes('profile')) {
          this.inProfile = true;
          this.inCatalog = false;
          this.inMarketplace = false;
        } else {
          this.inProfile = false;
          this.inCatalog = false;
          this.inMarketplace = false;
        }
      }
    });
  }

  ngOnInit() { }

  getProducts(nb): Observable<any>{
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = {
      headers: headers
    };
    return this.httpClient
      .post('http://25.85.144.149/RESTapi/web/app_dev.php/products/few', 'nb=' + nb, options);
  }

  getData(): Observable<any>{
    return this.httpClient
      .get<any[]>('http://25.85.144.149/RESTapi/web/app_dev.php/products');
  }

}
