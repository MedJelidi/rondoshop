import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

@Injectable()
export class BlockGuard implements CanActivate {

  isBlocked: boolean;

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (JSON.parse(localStorage.getItem('currentUserInfo')) != null) {
      this.isBlocked = JSON.parse(localStorage.getItem('currentUserInfo'))[0].blocked === 1;
    } else {
      this.isBlocked = true;
    }

    if (!this.isBlocked) {
      return true;
    }
    this.router.navigate(['/not-found']);
  }
}
