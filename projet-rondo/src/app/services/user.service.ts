import {User} from "../models/User.model";
import {Observable, Subject} from "rxjs";
import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class UserService {
  users: User[];

  loggedInUser: number;

  userSubject = new Subject<User[]>();

  constructor(private httpClient: HttpClient) { }

  getUser(id): Observable<any>{
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = {
      headers: headers
    };
    return this.httpClient
      .get('http://25.85.144.149/RESTapi/web/app_dev.php/profile/' + id, options);
  }

  getUserId() {

  }

}
