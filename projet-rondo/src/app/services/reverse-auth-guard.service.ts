import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Injectable} from "@angular/core";
import {AuthService} from "./auth.service";
import {LoginComponent} from "../login/login.component";
import {Observable} from "rxjs";

@Injectable()
export class ReverseAuthGuardService implements CanActivate {

  constructor(private authService: AuthService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (JSON.parse(localStorage.getItem('currentUser')) === null) {
      return true;
    }

    this.router.navigate(['/marketplace']);
  }
}
