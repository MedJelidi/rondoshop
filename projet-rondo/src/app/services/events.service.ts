import {Event} from "../models/Event.model";
import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class EventsService {

  constructor(private httpClient: HttpClient) { }

  events: Event[];
  eventProfileStyle: boolean = false;

  ngOnInit() { }

  getEvents(nb): Observable<any>{
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = {
      headers: headers
    };
    return this.httpClient
      .post('http://25.85.144.149/RESTapi/web/app_dev.php/events/few', 'nb=' + nb, options);
  }

  getData(): Observable<any>{
    return this.httpClient
      .get<any[]>('http://25.85.144.149/RESTapi/web/app_dev.php/events');
  }

}
