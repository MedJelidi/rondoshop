import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {LoginComponent} from "../login/login.component";

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  token: string;

  constructor(private httpClient: HttpClient,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.token = this.route.snapshot.params['token'];
    this.httpClient.get<any>('http://25.85.144.149/RESTapi/web/app_dev.php/register/confirm/' + this.token).subscribe(
      (response) => {
        console.log(response);
        LoginComponent.authStatus = true;
        localStorage.setItem('currentUser', JSON.stringify({ token: response.token }));
      },
    (error) => {
        console.log(error);
    }
    );
  }

}
