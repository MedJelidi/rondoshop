import { Component, OnInit } from '@angular/core';
import {ProductsService} from "../services/products.service";
import {Product} from "../models/Product.model";

@Component({
  selector: 'app-marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.css']
})
export class MarketplaceComponent implements OnInit {

  productsForAdmin: Product[];
  productsForUser: Product[];
  stillLoading: boolean = true;
  isAdmin: boolean;
  isBlocked: boolean;
  i: number = 8;
  notscrolly: boolean = true;
  notEmptyProduct: boolean = true;
  searchProducts;

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.productsService.getProducts(0).subscribe( products => {
      this.productsForUser = products.filter(x => x.valid === 1);
      this.productsForAdmin = products;

      this.stillLoading = false;
      if (JSON.parse(localStorage.getItem('currentUserInfo')) != null) {
        this.isAdmin = JSON.parse(localStorage.getItem('currentUserInfo'))[0].roles.includes("ROLE_ADMIN");
        this.isBlocked = JSON.parse(localStorage.getItem('currentUserInfo'))[0].blocked === 1;
      } else {
        this.isAdmin = false;
        this.isBlocked = false;
      }
    });
  }

  loadNextProducts() {
    this.productsService.getProducts(this.i)
      .subscribe( (products) => {
        if (products.length < 8) {
          this.notEmptyProduct = false;
          this.stillLoading = false;
        }
        this.productsForUser = this.productsForUser.concat(products.filter(x => x.valid === 1));
        this.productsForAdmin = this.productsForAdmin.concat(products);
        this.i = this.i + 8;
        this.notscrolly = true;
      });
  }

  onScroll() {
    if (this.notscrolly && this.notEmptyProduct) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextProducts();
    }
  }

  onCategory(cat) {
    this.searchProducts = cat;
  }

}
