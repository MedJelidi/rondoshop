import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-event-profile',
  templateUrl: './event-profile.component.html',
  styleUrls: ['./event-profile.component.css']
})
export class EventProfileComponent implements OnInit {

  @Input() id: number;
  @Input() creator: number;
  @Input() title: string;
  @Input() location: string;
  @Input() date: string;
  @Input() description: string;
  @Input() image: string;

  constructor() { }

  ngOnInit() {
  }

}
