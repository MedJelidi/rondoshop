import {Component, Input, OnInit} from '@angular/core';
import {ProductsService} from "../services/products.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Product} from "../models/Product.model";
import {ProductsComponent} from "../products/products.component";
import {NavigationStart, Router} from "@angular/router";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() id: number;
  @Input() name: string;
  @Input() price: string;
  @Input() date_pub: string;
  @Input() image: string;
  @Input() adminProduct: number;
  @Input() size: string;
  @Input() color: string;
  @Input() category: string;
  @Input() valid: boolean;

  inCatalog: boolean;
  inMarketplace: boolean;
  inProfile: boolean;
  isAdmin: boolean;
  edit: boolean;
  alreadyLiked: boolean = false;
  idUser: number;

  constructor(private productsService: ProductsService,
              private httpClient: HttpClient,
              private router: Router) {
    /*router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'] == '/products') {
          console.log("incatalog");
          this.inCatalog = true;
        } else {
          this.inMarketplace = true;
          console.log(this.inMarketplace);
        }
      }
    });*/
    this.edit = this.productsService.Edit;
    this.inCatalog = this.productsService.inCatalog;
    //console.log("incatalog: " + this.inCatalog);
    this.inMarketplace = this.productsService.inMarketplace;
    this.inProfile = this.productsService.inProfile;
    //console.log("inMarketplace: " + this.inMarketplace);
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('currentUserInfo')) != null) {
      this.idUser = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
    } else {
      this.idUser = null;
    }
    if (localStorage.getItem('currentUserInfo') != null) {
      this.isAdmin = JSON.parse(localStorage.getItem('currentUserInfo'))[0].roles.includes("ROLE_ADMIN");
    } else {
      this.isAdmin = false;
    }
  }

  onLike() {
    if (JSON.parse(localStorage.getItem('currentUserInfo')) === null) {
      this.router.navigate(['/login']);
    }
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = {
      headers: headers
    };
    this.httpClient.post('http://25.85.144.149/RESTapi/web/app_dev.php/posts/' + this.id + '/like', 'iduser=' + this.idUser, options)
      .subscribe(
        (response) => {
          console.log(response['nblikes']);
        },
        (error) => {
          console.log('Error while liking a post:');
          console.log(error);
          this.alreadyLiked = true;
        }
      );
  }

  onDelete() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/products/' + this.id + '/delete')
      .subscribe(
        (response) => {
          console.log(response);
          if (this.inCatalog) {
            this.router.navigate(['/products']);
          }
          this.router.navigate(['/marketplace']);
        },
        (error) => {
          console.log('Error while deleting a product: ' + error.toString());
        }
      );
  }

  onVerify() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/products/' + this.id + '/accept')
      .subscribe(
        (response) => {
          if (this.inCatalog) {
            this.router.navigate(['/products']);
          }
          this.router.navigate(['/marketplace']);
        },
        (error) => {
          console.log('Error while verifying a product: ' + error.toString());
        }
      );
  }

  onBlock() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/products/' + this.id + '/block')
      .subscribe(
        (response) => {
          if (this.inCatalog) {
            this.router.navigate(['/products']);
          }
          this.router.navigate(['/marketplace']);
        },
        (error) => {
          console.log('Error while blocking a product: ' + error.toString());
        }
      );
  }

}
