import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ProductsService} from "../services/products.service";
import {Product} from "../models/Product.model";

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})
export class SingleProductComponent implements OnInit {

  name: string = 'name9';
  price: string = 'price9';
  date_pub: string = 'December, 28, 2019';
  images: any[];
  propUsername: string;
  idProp: number;
  size: string;
  color: string;
  category: string = '';
  stillLoading: boolean = true;
  ville: string;
  imgUrl: string;

  products: Product[];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private productsService: ProductsService) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.productsService.getData().subscribe(serverProducts => {
      this.products = serverProducts;
      const product = this.products.find(
        (ProductObject) => {
          return ProductObject.id_product === +id;
        }
      );
      this.name = product.title;
      this.price = product.price;
      this.date_pub = product.date_pub;
      this.images = product.images;
      this.color = product.color;
      this.category = product.id_categorie.libelle;
      this.ville = product.city;
      this.size = product.size;
      this.propUsername = product.id_prop.username;
      this.idProp = product.id_prop.id;
      this.imgUrl = product.id_prop.image;
      this.stillLoading = false;
    });
  }

}
