import { Component, OnInit } from '@angular/core';
import {LoginComponent} from "../login/login.component";
import {Router} from "@angular/router";
import {ProductsService} from "../services/products.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  authStatus: boolean;
  idUser: number;

  constructor(private router: Router,
              private productsService: ProductsService) {
    LoginComponent.getAuth.subscribe(auth => this.changeAuth(auth));
  }

  ngOnInit() {
    this.authStatus = LoginComponent.authStatus;
    if (JSON.parse(localStorage.getItem('currentUserInfo')) != null) {
      this.idUser = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
    }
  }

  private changeAuth(auth: boolean): void {
    this.authStatus = auth;
  }

  onCatalog() {
    this.productsService.inCatalog = true;
  }

  onHome() {
    this.productsService.inCatalog = false;
    this.router.navigate(['']);
  }

  onSignOut() {
    this.authStatus = false;
    LoginComponent.authStatus = false;
    LoginComponent.logout();
  }

}
