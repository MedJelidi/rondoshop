import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { HostListener } from '@angular/core';
import {LoginComponent} from "./login/login.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  showSlider: boolean;
  appRoutes: any[] = [];
  public currentUser;

  constructor(private router: Router) {
    for (let i = 0 ; i < this.router.config.length ; i++) {
      this.appRoutes.push('/' + this.router.config[i].path);
    }
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'] == '/signup'
          || event['url'] == '/login'
          || event['url'] == '/single-product'
          || event['url'] == '/events'
          || event['url'].includes('products')
          || event['url'] == '/profile'
          || (event['url'].includes('/events') && (event['url'].length > 8))
          || event['url'] == '/not-found'
          || !this.appRoutes.includes(event['url'])
          || event['url'] == '/add-product'
          || event['url'] == '/add-post'
          || event['url'] == '/add-event'
          || event['url'] == '/marketplace'
          || event['url'] == '/lifestyle'
          || event['url'] == '/about-us'
          || event['url'].includes('/confirmation')) {
          this.showSlider = false;
        } else {
          this.showSlider = true;
        }
      }
    });
    LoginComponent.authStatus = !!localStorage.getItem('currentUser');
  }

  ngOnInit() { }

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    console.log('Back button pressed');
  }

}
