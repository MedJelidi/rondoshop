import {Component, Input, OnInit} from '@angular/core';
import {PostsService} from "../services/posts.service";
import {Post} from "../models/Post.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() posts: Post[];
  @Input() id: number;
  @Input() content: string;
  @Input() date_pub: string;
  @Input() attached_file: string;
  @Input() shared_post: any;
  @Input() numberOfLikes: number;
  @Input() valid: boolean;
  @Input() adminPost: boolean;

  title: string;
  idUser: number;
  alreadyLiked: boolean = false;
  isAdmin: boolean;

  constructor(private httpClient: HttpClient,
              private router: Router) { }

  ngOnInit() {
    const postObject = this.posts.find(
      (PostObject) => {
        return PostObject.id === this.id;
      });
    if (this.shared_post != null) {
      this.title = postObject.id_user.username + " has shared.";
    } else {
      this.title = postObject.id_user.username + " has posted.";
    }
    if (JSON.parse(localStorage.getItem('currentUserInfo')) != null) {

    }
    if (localStorage.getItem('currentUserInfo') != null) {
      this.isAdmin = JSON.parse(localStorage.getItem('currentUserInfo'))[0].roles.includes("ROLE_ADMIN");
      this.idUser = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
    } else {
      this.isAdmin = false;
      this.idUser = null;
    }
  }

  onLike() {
    if (JSON.parse(localStorage.getItem('currentUserInfo')) === null) {
      this.router.navigate(['/login']);
      return;
    }
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = {
      headers: headers
    };
    this.httpClient.post('http://25.85.144.149/RESTapi/web/app_dev.php/posts/' + this.id + '/like', 'iduser=' + this.idUser, options)
      .subscribe(
        (response) => {
          console.log(response['nblikes']);
          this.numberOfLikes = response['nblikes'];
        },
        (error) => {
          console.log('Error while liking a post:');
          console.log(error);
          this.alreadyLiked = true;
        }
      );
  }

  onDelete() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/posts/' + this.id + '/delete')
      .subscribe(
        (response) => {
          console.log(response);
          this.router.navigate(['/lifestyle']);
        },
        (error) => {
          console.log('Error while deleting a post: ' + error.toString());
        }
      );
  }

  onVerify() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/posts/' + this.id + '/accept')
      .subscribe(
        (response) => {
          this.router.navigate(['/lifestyle']);
        },
        (error) => {
          console.log('Error while verifying a post: ' + error.toString());
        }
      );
  }

  onBlock() {
    this.httpClient.get('http://25.85.144.149/RESTapi/web/app_dev.php/posts/' + this.id + '/block')
      .subscribe(
        (response) => {
          this.router.navigate(['/lifestyle']);
        },
        (error) => {
          console.log('Error while blocking a post: ' + error.toString());
        }
      );
  }

}
