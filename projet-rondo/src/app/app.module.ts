import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule, Routes } from "@angular/router";
import { EventsComponent } from './events/events.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from "./services/auth.service";
import { SingleEventComponent } from './single-event/single-event.component';
import { EventsService } from "./services/events.service";
import { EventComponent } from './event/event.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthGuard } from "./services/auth-guard.service";
import { BlockGuard } from "./services/block-guard.service";
import { ProfileComponent } from './profile/profile.component';
import { SliderComponent } from './slider/slider.component';
import { ProductComponent } from './product/product.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ProductsService } from "./services/products.service";
import { EventProfileComponent } from './event-profile/event-profile.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserService } from "./services/user.service";
import {HttpClientModule} from "@angular/common/http";
import { SingleProductComponent } from './single-product/single-product.component';
import { ProductsComponent } from './products/products.component';
import { FollowUsComponent } from './follow-us/follow-us.component';
import { HomeComponent } from './home/home.component';
import { AddProductComponent } from './add-product/add-product.component';
import { MarketplaceComponent } from './marketplace/marketplace.component';
import { LoadingComponent } from './loading/loading.component';
import { AddEventComponent } from './add-event/add-event.component';
import { LifestyleComponent } from './lifestyle/lifestyle.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { AddPostComponent } from './add-post/add-post.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { PostComponent } from './post/post.component';
import {PostsService} from "./services/posts.service";
import { SinglePostComponent } from './single-post/single-post.component';
import { UserComponent } from './user/user.component';
import { FullLoadingComponent } from './full-loading/full-loading.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'events', component: EventsComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'marketplace', component: MarketplaceComponent },
  { path: 'add-product', canActivate: [BlockGuard, AuthGuard], component: AddProductComponent },
  { path: 'add-post', canActivate: [BlockGuard, AuthGuard], component: AddPostComponent },
  { path: 'add-event', canActivate: [BlockGuard, AuthGuard], component: AddEventComponent },
  { path: 'events/:id', component: SingleEventComponent },
  { path: 'user/:id', component: UserComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent },
  { path: 'lifestyle', component: LifestyleComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'edit-profile', canActivate: [AuthGuard], component: EditProfileComponent },
  { path: 'confirmation/:token', component: ConfirmationComponent },
  { path: 'products/:id', component: SingleProductComponent },
  { path: 'lifestyle/:id', component: SinglePostComponent },
  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    EventsComponent,
    SignupComponent,
    LoginComponent,
    SingleEventComponent,
    EventComponent,
    FourOhFourComponent,
    ProfileComponent,
    SliderComponent,
    ProductComponent,
    EditProfileComponent,
    EventProfileComponent,
    SingleProductComponent,
    ProductsComponent,
    FollowUsComponent,
    HomeComponent,
    AddProductComponent,
    MarketplaceComponent,
    LoadingComponent,
    AddEventComponent,
    LifestyleComponent,
    AboutUsComponent,
    AddPostComponent,
    ConfirmationComponent,
    PostComponent,
    SinglePostComponent,
    UserComponent,
    FullLoadingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    InfiniteScrollModule,
    Ng2SearchPipeModule
  ],
  providers: [
    AuthService,
    EventsService,
    AuthGuard,
    BlockGuard,
    ProductsService,
    UserService,
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
