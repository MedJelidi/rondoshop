import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() static getAuth: EventEmitter<any> = new EventEmitter();

  static token: string;
  static authStatus: boolean;
  loading: boolean = false;
  notVerified: boolean;

  userForm: FormGroup;
  incorrectData: boolean = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.userForm = this.formBuilder.group(
      {
        userName: ['', Validators.required],
        password: ['', Validators.required]
      }
    );
  }

  onSignIn(): Observable<boolean> {
    this.notVerified = false;
    this.incorrectData = false;
    this.loading = true;
    const formSignInValue = this.userForm.value;
    const userName = formSignInValue['userName'];
    const password = formSignInValue['password'];
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = {
      headers: headers
    };
    this.httpClient.post<any>('http://25.85.144.149/RESTapi/web/app_dev.php/api/login_check', "_username="+userName+"&_password="+password, options)
      .subscribe(
        (response) => {
          const token = response.token;
          LoginComponent.token = token;
          //console.log("token: " + token);
          localStorage.setItem('currentUser', JSON.stringify({ token: token }));
          localStorage.setItem('auth', 'true');
          LoginComponent.authStatus = true;
          LoginComponent.getAuth.emit(true);
          this.router.navigate(['']);
          return of(true);
        },
        (error) => {
          this.loading = false;
          if (error.error.message === 'Account is disabled.') {
            this.notVerified = true;
          } else {
            this.incorrectData = true;
          }
        }
      );
    LoginComponent.getAuth.emit(false);
    return of(false);
  }

  static logout(): void {
    // clear token remove user from local storage to log user out
    LoginComponent.getAuth.emit(false);
    LoginComponent.token = null;
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentUserInfo');
  }
}

