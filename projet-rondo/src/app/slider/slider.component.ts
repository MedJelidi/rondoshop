import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit() { }

  @HostListener('unloaded')
  ngOnDestroy(): void {
    console.log('destroyed');
  }

}
