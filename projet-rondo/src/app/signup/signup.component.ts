import { Component, OnInit } from '@angular/core';
import {LoginComponent} from "../login/login.component";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {of} from "rxjs";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {

  userForm: FormGroup;
  incorrectData: boolean = false;
  loading: boolean = false;
  submitted: boolean;
  differentPassword: boolean;
  usernameExist: boolean;
  emailExist: boolean;
  phoneExist: boolean;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,
              private httpClient: HttpClient) { }

  ngOnInit() {
    this.initForm();
  }

  get f() { return this.userForm.controls; }

  initForm() {
    this.userForm = this.formBuilder.group(
      {
        username: ['med_jelidi', Validators.required],
        email: ['email@gmail.com', [Validators.required, Validators.email]],
        password: ['12345678', [Validators.required, Validators.minLength(8)]],
        confPassword: ['12345678', Validators.required],
        phoneNumber: ['22222222', [Validators.required, Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")]],
        gender: ['Male', Validators.required],
        city: ['Kram', Validators.required],
        image: ['']
      }
    );
  }

  onSignUp() {
    this.usernameExist = false;
    this.emailExist = false;
    this.phoneExist = false;
    this.differentPassword = false;
    this.submitted = true;
    if (this.userForm.invalid) {
      return;
    }
    const formSignUpValue = this.userForm.value;
    const username = formSignUpValue['username'];
    const password = formSignUpValue['password'];
    const confPassword = formSignUpValue['confPassword'];
    if (password != confPassword) {
      this.differentPassword = true;
      return;
    }
    const email = formSignUpValue['email'];
    const phoneNumber = formSignUpValue['phoneNumber'];
    const gender = formSignUpValue['gender'];
    const city = formSignUpValue['city'];
    this.loading = true;
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = {
      headers: headers
    };
    this.httpClient.post<any>('http://25.85.144.149/RESTapi/web/app_dev.php/register/',
      "username=" + username
      + "&password=" + password
      + "&email=" + email
      + "&phone="+ phoneNumber
      + "&gender=" + gender
      + "&city=" + city
      + "&image=" + '', options)
      .subscribe(
        (response) => {
          this.router.navigate(['']);
          return of(true);
        },
        (error) => {
          //console.log('Error while fetching token from server: ' + error);
          if (error.error.errorExist === 1) {
            this.usernameExist = true;
          } else if (error.error.errorExist === 2) {
            this.emailExist = true;
          } else if (error.error.errorExist === 3) {
            this.phoneExist = true;
          }
          this.loading = false;
        }
      );
    this.incorrectData = true;
    LoginComponent.getAuth.emit(false);
    return of(false);
  }

}
