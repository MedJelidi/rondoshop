import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ProductsService} from "../services/products.service";
import {EventsService} from "../services/events.service";
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {Product} from "../models/Product.model";
import {Event} from "../models/Event.model";
import {Post} from "../models/Post.model";
import {PostsService} from "../services/posts.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {LoginComponent} from "../login/login.component";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {

  userName: string = 'UserName';
  phoneNumber: string = '28249424';
  gender: string = 'Male';
  date: string = '2018-04-25';
  city: string;
  onEditClick: boolean = false;
  onViewProductsClick: boolean = false;
  onViewEventsClick: boolean = false;
  onViewPostsClick: boolean = false;
  stillLoading: boolean = true;
  lastLogin: string;
  imgUrl: string = '/assets/img/icons/profile-picture.png';
  fileToUpload: File;
  editForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  errorEditing: boolean;
  usernameExist: boolean;
  phoneExist: boolean;
  changesSaved: boolean;

  verifiedProducts: Product[];
  unverifiedProducts: Product[];
  verifiedEvents: Event[];
  unverifiedEvents: Event[];
  verifiedPosts: Post[];
  unverifiedPosts: Post[];
  incorrectData: boolean;
  correctData: boolean;
  i_products: number = 8;
  i_events: number = 8;
  i_posts: number = 8;
  onEditUsername: boolean;
  onEditPhone: boolean;
  onEditGender: boolean;
  onEditPicture: boolean;

  notscrolly: boolean = true;
  notEmptyProduct: boolean = true;
  notEmptyEvent: boolean = true;
  notEmptyPost: boolean = true;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private httpClient: HttpClient,
              private productsService: ProductsService,
              private eventsService: EventsService,
              private postsService: PostsService,
              private userService: UserService) {

    this.productsService.getProducts(0).subscribe(
      (data) => {
        const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
        this.verifiedProducts = data.filter(x => x.id_prop.id === idProp && x.valid === 1);
        this.unverifiedProducts = data.filter(x => x.id_prop.id === idProp && x.valid === 0);
        this.stillLoading = false;
      }
    );

    this.eventsService.getEvents(0).subscribe(
      (data) => {
        const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
        this.verifiedEvents = data.filter(x => x.id_prop.id === idProp && x.valid === 1);
        this.unverifiedEvents = data.filter(x => x.id_prop.id === idProp && x.valid === 0);
        this.stillLoading = false;
      }
    );

    this.postsService.getPosts(0).subscribe(
      (data) => {
        const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
        this.verifiedPosts = data.filter(x => x.id_prop.id === idProp && x.valid === 1);
        this.unverifiedPosts = data.filter(x => x.id_prop.id === idProp && x.valid === 0);
        this.stillLoading = false;
      }
    );

    this.userName = JSON.parse(localStorage.getItem('currentUserInfo'))[0].username;
    this.phoneNumber = JSON.parse(localStorage.getItem('currentUserInfo'))[0].phone;
    this.gender = JSON.parse(localStorage.getItem('currentUserInfo'))[0].gender;
    this.city = JSON.parse(localStorage.getItem('currentUserInfo'))[0].city;

    if (JSON.parse(localStorage.getItem('currentUserInfo'))[0].last_login === null) {
      this.lastLogin = new Date().getTime().toString();
    } else {
      this.lastLogin = JSON.parse(localStorage.getItem('currentUserInfo'))[0].last_login;
    }
    this.imgUrl = JSON.parse(localStorage.getItem('currentUserInfo'))[0].image;

  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.editForm = this.formBuilder.group(
      {
        username: [this.userName, Validators.required],
        phoneNumber: [this.phoneNumber, [Validators.required, Validators.minLength(8), Validators.maxLength(8), Validators.pattern("^[0-9]*$")]],
        gender: [this.gender, Validators.required],
        city: [this.city, Validators.required]
        //password: ['', Validators.required]
      }
    );
  }

  handleFileInput(files: any) {
    this.fileToUpload = files.target.files[0];
    if (files.target.files && files.target.files[0]) {
      let reader = new FileReader();

      reader.readAsDataURL(files.target.files[0]); // read file as data url

      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.imgUrl = event.target.result;
        this.onEditPicture = true;
      }
    }
  }

  onViewProducts() {
    this.onViewProductsClick = !this.onViewProductsClick;
    this.onViewEventsClick = false;
    this.onViewPostsClick = false;
    this.onEditClick = false;
    this.productsService.Edit = true;
  }

  onViewEvents() {
    this.onViewEventsClick = !this.onViewEventsClick;
    this.onViewProductsClick = false;
    this.onViewPostsClick = false;
    this.onEditClick = false;
    this.eventsService.eventProfileStyle = true;
  }

  onViewPosts() {
    this.onViewPostsClick = !this.onViewPostsClick;
    this.onViewProductsClick = false;
    this.onViewEventsClick = false;
    this.onEditClick = false;
    this.postsService.Edit = true;
  }

  onEdit() {
    this.onEditClick = true;
    this.onViewProductsClick = false;
    this.onViewEventsClick = false;
    this.onViewPostsClick = false;
  }

  onCancel() {
    this.onEditClick = false;
  }

  get f() { return this.editForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.changesSaved = false;
    this.usernameExist = false;
    this.phoneExist = false;
    if (this.editForm.invalid) {
      return;
    }
    const formEditValue = this.editForm.value;
    const username = formEditValue['username'];
    const phoneNumber = formEditValue['phoneNumber'];
    const gender = formEditValue['gender'];
    const city = formEditValue['city'];
    //const password = formEditValue['password'];
    let token = JSON.parse(localStorage.getItem('currentUser')).token;
    //const city = JSON.parse(localStorage.getItem('currentUserInfo'))[0].city;

    console.log(token);

    const formData = new FormData();
    formData.append('username', username);
    formData.append('phone', phoneNumber);
    formData.append('gender', gender);
    formData.append('password', '');
    formData.append('token', token);
    formData.append('city', city);

    if (typeof this.fileToUpload === 'undefined') {
      formData.append('image', '');
    } else {
      formData.append('image', this.fileToUpload);
    }
    this.loading = true;
    this.httpClient.post('http://25.85.144.149/RESTapi/web/app_dev.php/profile/edit', formData)
      .subscribe(
        (response) => {
          let newToken = response['token'];
          this.loading = false;
          this.changesSaved = true;
          this.userName = username;
          let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

          let options = {
            headers: headers
          };
          this.httpClient.post<any>('http://25.85.144.149/RESTapi/web/app_dev.php/getuser', "token=" + response['token'], options)
            .subscribe(
              (response) => {
                localStorage.setItem('currentUserInfo', JSON.stringify(response));
                LoginComponent.token = response.token;
                localStorage.setItem('currentUser', JSON.stringify({ token: newToken }));
                //this.correctData = true;
                //console.log(JSON.parse(localStorage.getItem('currentUserInfo'))[0].email);
              },
              (error) => {
                console.log('Error while fetching token from server: ' + error);
              }
            );
        },
        (error) => {
          this.loading = false;
          console.log('Error while editing user settings: ' + error);
          if (error.error.errorExist === 1) {
            this.usernameExist = true;
            return;
          } else if (error.error.errorExist === 3) {
            this.phoneExist = true;
            return;
          }
          this.errorEditing = true;
        }
      );
  }

  loadNextProducts() {
    this.productsService.getProducts(this.i_products)
      .subscribe( (products) => {
        if (products.length < 8) {
          this.notEmptyProduct = false;
          this.stillLoading = false;
        }
        const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
        this.verifiedProducts = this.verifiedProducts.concat(products.filter(x => x.id_prop.id === idProp && x.valid === 1));
        this.unverifiedProducts = this.unverifiedProducts.concat(products.filter(x => x.id_prop.id === idProp && x.valid === 0));
        this.i_products = this.i_products + 8;
        this.notscrolly = true;
      });
  }

  onScrollProducts() {
    if (this.notscrolly && this.notEmptyProduct) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextProducts();
    }
  }

  loadNextEvents() {
    this.eventsService.getEvents(this.i_events)
      .subscribe( (events) => {
        if (events.length < 8) {
          this.notEmptyEvent = false;
          this.stillLoading = false;
        }
        const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
        this.verifiedEvents = this.verifiedEvents.concat(events.filter(x => x.id_prop.id === idProp && x.valid === 1));
        this.unverifiedEvents = this.unverifiedEvents.concat(events.filter(x => x.id_prop.id === idProp && x.valid === 0));
        this.i_events = this.i_events + 8;
        this.notscrolly = true;
      });
  }

  onScrollEvents() {
    if (this.notscrolly && this.notEmptyEvent) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextEvents();
    }
  }

  loadNextPosts() {
    this.postsService.getPosts(this.i_posts)
      .subscribe( (posts) => {
        if (posts.length < 8) {
          this.notEmptyPost = false;
          this.stillLoading = false;
        }
        const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;
        this.verifiedPosts = this.verifiedPosts.concat(posts.filter(x => x.id_prop.id === idProp && x.valid === 1));
        this.unverifiedPosts = this.unverifiedPosts.concat(posts.filter(x => x.id_prop.id === idProp && x.valid === 0));
        this.i_posts = this.i_posts + 8;
        this.notscrolly = true;
      });
  }

  onScrollPosts() {
    if (this.notscrolly && this.notEmptyPost) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextPosts();
    }
  }

}
