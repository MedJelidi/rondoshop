import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {Post} from "../models/Post.model";

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  postForm: FormGroup;
  fileToUpload: File[];
  loading: boolean;
  errorAdding: boolean = false;
  submitted: boolean;
  noPicture: boolean;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private httpClient: HttpClient) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.postForm = this.formBuilder.group(
      {
        content: ['The description of the post.', Validators.required],
        'post[images][]': ['', Validators.required]
      }
    );
  }

  handleFileInput(files: any) {
    this.fileToUpload = files.target.files;
  }

  onSubmit() {
    this.submitted = true;
    this.noPicture = typeof this.fileToUpload === 'undefined';
    if (this.postForm.invalid) {
      return;
    }
    const formAddPostValue = this.postForm.value;
    const content = formAddPostValue['content'];
    this.loading = true;
    const idProp = JSON.parse(localStorage.getItem('currentUserInfo'))[0].id;

    const formData = new FormData();
    formData.append('content', content);
    for(let i = 0 ; i < this.fileToUpload.length ; i++) {
      formData.append('post[images][]', this.fileToUpload[i]);
    }
    formData.append('idProp', idProp);

    this.httpClient.post<Post>('http://25.85.144.149/RESTapi/web/app_dev.php/posts/new', formData)
      .subscribe(
        (response) => {
          console.log(response);
          this.router.navigate(['lifestyle']);
        },
        (error) => {
          console.log('Error while adding post to server: ' + error);
          this.errorAdding = true;
          this.loading = false;
        }
      );
  }

}
