import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EventsService} from "../services/events.service";
import {Event} from "../models/Event.model";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-single-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.css']
})
export class SingleEventComponent implements OnInit {

  events: Event[];

  titre: string;
  description: string;
  date: string;
  location: string;
  image: string;
  creator: string;
  idCreator: number;

  stillLoading: boolean = true;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private eventsService: EventsService,
              private httpClient: HttpClient) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.eventsService.getData().subscribe(serverEvents => {
      this.events = serverEvents;
      const event = this.events.find(
        (ProductObject) => {
          return ProductObject.id_event === +id;
        }
      );
      this.titre = event.nom;
      this.description = event.description;
      this.location = event.lieu;
      this.date = event.dateheure_deb;
      this.image = event.image;
      this.creator = event.id_prop.username;
      this.idCreator = event.id_prop.id;
      this.stillLoading = false;
    });
  }

  onLike() {
    const id = this.route.snapshot.params['id'];
    this.httpClient.post<Event>('http://25.85.144.149/RESTapi/web/app_dev.php/products/new', id)
      .subscribe(
        (response) => {
          console.log(response);
          //this.productsService.getProductsFromServer();
        },
        (error) => {
          console.log('Error while liking event: ' + error);
        }
      );
  }

}
