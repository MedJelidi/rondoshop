import { Component, OnInit } from '@angular/core';
import {PostsService} from "../services/posts.service";
import {ActivatedRoute} from "@angular/router";
import {Post} from "../models/Post.model";
import {Event} from "../models/Event.model";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {

  posts: Post[];

  username: string;
  content: string;
  date_pub: string;
  attached_file: string;
  stillLoading: boolean = true;
  idUser: number;
  numberOfLikes: number;

  constructor(private postsService: PostsService,
              private route: ActivatedRoute,
              private httpClient: HttpClient) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.postsService.getData().subscribe(posts => {
      this.posts = posts;
      const post = this.posts.find(
        (PostObject) => {
          return PostObject.id === +id;
        }
      );

      this.idUser = post.id_user.id;
      this.username = post.id_user.username;
      this.content = post.content;
      this.date_pub = post.date_pub;
      this.attached_file = post.attached_file;
      this.numberOfLikes = post.like_count;
      this.stillLoading = false;
    });
  }

  onLike() {
    const id = this.route.snapshot.params['id'];
    this.httpClient.post<Event>('http://25.85.144.149/RESTapi/web/app_dev.php/products/new', id)
      .subscribe(
        (response) => {
          console.log(response);
          //this.productsService.getProductsFromServer();
        },
        (error) => {
          console.log('Error while liking event: ' + error);
        }
      );
  }

}
