import { Component, OnInit } from '@angular/core';
import {Product} from "../models/Product.model";
import {ProductsService} from "../services/products.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products: Product[];

  stillLoading: boolean = true;

  constructor(private productsService: ProductsService,
              private httpClient: HttpClient) { }

  ngOnInit() {
    this.productsService.getProducts(0).subscribe(serverProducts => {
      console.log(serverProducts);
      this.products = serverProducts.filter(x => x.valid === 1);
      this.stillLoading = false;
    });

    let token;

    if (JSON.parse(localStorage.getItem('currentUser')) != null) {
      token = JSON.parse(localStorage.getItem('currentUser')).token;
    } else {
      token = null;
    }
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

    let options = {
      headers: headers
    };

    this.httpClient.post<any>('http://25.85.144.149/RESTapi/web/app_dev.php/getuser', "token=" + token, options)
      .subscribe(
        (response) => {
          localStorage.setItem('currentUserInfo', JSON.stringify(response));
          //console.log(JSON.parse(localStorage.getItem('currentUserInfo'))[0].email);
        },
        (error) => {
          console.log('Error while fetching token from server: ' + error);
        }
      );
  }

}
