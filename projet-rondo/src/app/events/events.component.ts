import {Component, Input, OnInit} from '@angular/core';
import {EventsService} from "../services/events.service";
import {Event} from "../models/Event.model";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  events: Event[];
  stillLoading: boolean = true;
  isBlocked: boolean;
  i: number = 8;
  notscrolly: boolean = true;
  notEmptyEvent: boolean = true;
  searchEvents;

  constructor(private eventsService: EventsService) {
    this.eventsService.eventProfileStyle = false;
  }

  ngOnInit() {
    this.eventsService.getEvents(0).subscribe(e => {
      this.events = e.filter(x => x.valid === 1);
      this.stillLoading = false;
      if (localStorage.getItem('currentUserInfo') != null) {
        this.isBlocked = JSON.parse(localStorage.getItem('currentUserInfo'))[0].blocked === 1;
      } else {
        this.isBlocked = false;
      }
    });
  }

  loadNextEvents() {
    this.eventsService.getEvents(this.i)
      .subscribe( (events) => {
        if (events.length < 8) {
          this.notEmptyEvent = false;
          this.stillLoading = false;
        }
        this.events = this.events.concat(events.filter(x => x.valid === 1));
        this.i = this.i + 8;
        this.notscrolly = true;
      });
  }

  onScroll() {
    if (this.notscrolly && this.notEmptyEvent) {
      this.stillLoading = true;
      this.notscrolly = false;
      this.loadNextEvents();
    }
  }

}
