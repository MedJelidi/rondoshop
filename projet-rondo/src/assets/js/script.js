$(document).ready(function(){
    $('.slider-one')
      .not(".slick-initialized")
      .slick({
        autoplay:true,
        autoplaySpeed:30000,
        dots:true,
        prevArrow:".site-slider .slider-btn .prev",
        nextArrow:".site-slider .slider-btn .next"
      });
    $('.slider-two')
      .not(".slick-initialized")
      .slick({
        prevArrow:".site-slider-two .prev",
        nextArrow:".site-slider-two .next",
        slidesToShow:5,
        slidesToScroll:1,
        autoplay:true,
        autoplaySpeed:3000
    })
      if (window.location.href.length <= 23) {
      $('.slider-one').slick('refresh');
    }

    var countDownDate = new Date("dec,31,2019").getTime();
    var countDownfuncton = setInterval(function () {
      var now = new Date().getTime();
      var distance = countDownDate - now;
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      document.getElementById("demo").innerHTML = days + "D:" + hours + "H:" + minutes + "M:" + seconds + "S";
      if (distance <0) {
        clearInterval(countDownfuncton);
        document.getElementById("demo").innerHTML = "EXPIRED";
      }

    },1000);





  });